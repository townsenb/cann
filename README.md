# CANN  (Work in progess!)
### C Artificial Neural Network
### Ben Townsend
# Required Packages
- libncurses-dev
- gcc
- make

# Install and Run
- `git clone https://gitlab.com/townsenb/cann && cd cann`
- `make`
- `./cann`
