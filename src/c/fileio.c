#include <cann.h>

#include <fileio.h>
#include <netutils.h>
#include <structure.h>
#include <gui.h>

void saveNet(char* filename){
    FILE *save = fopen(filename,"w+");
    if(save == NULL)    {return;}
    fprintf(save,"%d\n",Network.numLayers);
    for(int l=0; l<Network.numLayers;l++){
        fprintf(save,"%d\n",Network.layerCount[l]);
    }

    for(int i=0;i<Network.numLayers;i++){
        for(int j=0;j<Network.layerCount[i];j++){
            for(int e=0;e<Network.net[i][j].numOutgoing;e++){
                fprintf(save,"%.14f\n",Network.net[i][j].outgoingEdges[e]->weight);
            }
        }
    }
    fclose(save);
}

void exitdir(char** fileNames, DIR* d, int count){
    for(int i=0;i<count;i++){
        free(fileNames[i]);
    }
    free(fileNames);
    closedir(d);
}

struct fileNameData* loadFileNames(){
    int dirMaxSize = 100;
    DIR *d;
    struct dirent *dir;
    d = opendir("./networks/");
    char** fileNames = (char**) malloc(dirMaxSize);
    int count = 0;
    if (d){
        while ((dir = readdir(d)) != NULL){
            char tmp[25];
            strcpy(tmp,dir->d_name);
            char* filetype = malloc(4);
            strcpy(filetype,&tmp[strlen(dir->d_name)-3]);
            if(strcmp(filetype,".nn") == 0){
                fileNames[count] = (char*) malloc(strlen(dir->d_name)+1);
                strcpy(fileNames[count],dir->d_name);
                count++;
            }
            free(filetype);
        }
        if(count == 0){
            exitdir(fileNames,d,count);
            return NULL;
        }
    }

    struct fileNameData *result = malloc(sizeof(struct fileNameData));
    result->count = count;
    result->fileNames = fileNames;
    result->dir = d;

    return result;
}

bool loadNet(char* filename){
    if(!filename){
        return false;
    }
    char path[60] = "networks/";
    strcat(path,filename);
    FILE *loadFile = fopen(path,"r");
	if(!loadFile){
        return false;
	}

    char* line = (char*) malloc(15);
    size_t read;
    size_t len = 15;
    Network.totalEdges = 0;

    int numLayers = 0;
    int* layerCounts = NULL;

    int count = 0;
    while ((read = getline(&line, &len, loadFile)) != (lui)-1) {
        if(count == 0){
            numLayers = atoi(line);
            layerCounts = calloc(numLayers, sizeof(int));
        }else if(count < numLayers){
            layerCounts[count-1] = atoi(line);
        }else{
            layerCounts[count-1] = atoi(line);
            setupLayers(numLayers,layerCounts);
            build();
            loadWeights(loadFile);
        }
        count++;
    }
    free(line);
    fclose(loadFile);
    Network.numLayers = Network.hiddenLayers + 2;
    return true;
}


void loadWeights(FILE* file){
    char* line = (char*) malloc(15);
    size_t read;
    size_t len = 15;
    for(int i=0;i<Network.numLayers;i++){
        for(int j=0;j<Network.layerCount[i];j++){
            for(int e=0;e<Network.net[i][j].numOutgoing;e++){
                if((read = getline(&line, &len, file)) != (lui)-1){
                    Network.net[i][j].outgoingEdges[e]->weight = atof(line); 
                }
            }
        }
    }   
}

int loadDataFile(char* file){
    int expectedVal = 0;

    FILE* loadFile = fopen(file,"r");
    char* line = (char*) malloc(15);
    size_t read;
    size_t len = 15;

    int count = 0;
    while ((read = getline(&line, &len, loadFile)) != (lui)-1) {
        if(count == 0){
            expectedVal = atoi(line);
        }else{
            //set input node values
            Network.net[0][count-1].val = atof(line);
        }
        count++;
    }
    free(line);
    fclose(loadFile);
    return expectedVal;
}
