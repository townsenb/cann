#include <cann.h>

#include <fileio.h>
#include <netutils.h>
#include <structure.h>
#include <gui.h>


void updateWeight(edge* e, double weight){
    e->weight = weight;
}

float sigmoid(float x){
    float e = 2.7182818284590;
    return 1 / (1+pow(e,x));
}

float weightedSum(node* n){
    float total = 0.0;
    for(int synapse = 0; synapse < n->numIncoming; synapse++){
        total += (n->incomingEdges[synapse]->fromNode->val * n->incomingEdges[synapse]->weight);
    }
    return sigmoid(total+Network.bias);
}

void fireNode(node* n, bool state){
    for(int synapse = 0; synapse < n->numOutgoing; synapse++){
        n->outgoingEdges[synapse]->firing = state;
    }
}

char* bytesToReadable(long b){
    double bytes = (double) b;
    char unit = 'B';
    if(bytes >= 1000.0){  //Kilobytes
        unit = 'K';
        bytes /= 1000.0;
    }
    if(bytes >= 1000.0){ //Megabytes
        unit = 'M';
        bytes /= 1000.0;
    }
    if(bytes >= 1000.0){ //Gigabytes
        unit = 'G';
        bytes /= 1000.0;
    }

    char* result = malloc(100 * sizeof(char));
    sprintf(result,"%.2f %c",bytes,unit);
    return result;
}

float charToFloat(char c){
    float f = (float)c;
    f /= CHAR_MAX;
    return f;
}

char floatToChar(float f){
    float c = f * CHAR_MAX;
    return (char)c;
}

float* stringToFloatArr(char* str){
    int size = strlen(str);
    float* result = malloc(sizeof(float) * size);
    for(int i=0; i<size; i++){
        result[i] = charToFloat(str[i]);
    }
    return result;
}


char* floatArrToString(float* fptr, int size){
    char* str = malloc(sizeof(char) * (size));
    for(int i=0; i<size; i++){
        str[i] = floatToChar(fptr[i]);
    }
    return str;
}

void randomizeAllWeights(){
    srand(time(NULL));
    double n = 0;
    for(int i=0;i<Network.numLayers;i++){
        for(int j=0;j<Network.layerCount[i];j++){
			Network.net[i][j].val = 0.0;
            for(int k=0;k<Network.net[i][j].numOutgoing;k++){
                n = (double)rand()/RAND_MAX*2.0-1.0; //float in range -1 to 1
                updateWeight(Network.net[i][j].outgoingEdges[k],n);
            }
        }
    }
}


void randomInputs(){
    float* arr = malloc(sizeof(float) * Network.inputs);
    for(int i=0; i<Network.inputs; i++){
        arr[i] = (float)rand() / RAND_MAX * 2.0 - 1.0;
    }
    setInputs(Network.inputs,arr);
}

bool setInputs(int numInputs, float* inputArr){
    for(int i=0; i<numInputs; i++){
        Network.net[0][i].val = inputArr[i];
    }

    //set unused input to 0
    for(int i=numInputs; i<Network.inputs; i++){
        Network.net[0][i].val = 0.0;
    }

    free(inputArr);
    return true;
}


bool compareOutputs(float* expected, float range){
    float diff = 0.0;
    for(int i=0; i<Network.outputs; i++){
        diff = Network.net[Network.numLayers-1][i].val - expected[i];
        if(fabs(diff) > range){
            return false;
        }
    }
    return true;
}

void adjust(node* n, float delta){
    //alert(NO_COLOR, "ID: %d", n->id);
    if (n->numIncoming != 0){ //not input
        edge* tmpedge;
        for(int e=0; e < n->numIncoming; e++){
            tmpedge = n->incomingEdges[e];
            adjust(tmpedge->fromNode, delta);
            tmpedge->weight -= (Network.learnRate * delta * tmpedge->fromNode->val);
        }
    }
    return;
}

void backprop(float* expected){
    float delta;
    for(int i=0; i < Network.outputs; i++){
        node* tmpnode = &Network.net[Network.numLayers-1][i];
        delta = expected[i] - tmpnode->val;
        adjust(tmpnode, delta);
    }
}

void step(int layer){
    for (int i = 0; i < Network.layerCount[layer]; i++){
        Network.net[layer][i].val = weightedSum(&Network.net[layer][i]);
    }
}

void train(float* inputs, float* expected){
    setInputs(Network.inputs, inputs);
    compareOutputs(expected, 0.1);
    for(int l=1; l<Network.numLayers; l++){
        step(l);
    }
    backprop(expected);
}

char* calcInput(float* inputs, int size){
    setInputs(size, inputs);
    for(int i=0; i<Network.numLayers; i++){
        step(i);
    }

    char* outs = malloc(sizeof(char) * Network.outputs);
    for(int i=0; i<Network.outputs; i++){
        outs[i] = floatToChar(Network.net[Network.numLayers-1][i].val);
    }

    return outs;
}
