#include <cann.h>

#include <fileio.h>
#include <netutils.h>
#include <structure.h>
#include <gui.h>
#include <ncursesutils.h>

#define listNum 8
#define menuWidth 24

#define BUILD_NEW 0
#define SAVE 1
#define LOAD 2
#define INPUT 3
#define TRAIN 4
#define VIEW 5
#define SETTINGS 6
#define QUIT 7

#define CONSTANT 0
#define TAPER 1
#define INDIV 2

int yMax, xMax;

bool netLoaded = false;


WINDOW *createSecondary(char *title, int height, int width, int x, int y){
	WINDOW *win = newwin(height, width, y, x);
	box(win, 0, 0);
	refresh();
	wprintw(win, title);
	wrefresh(win);
	return win;
}

WINDOW* textBox(bool pause, int textColor, char* fmt, ...){

	if(has_colors()) initColor();

	va_list args;
	va_start(args,fmt);
	char* str = calloc(120,sizeof(char));
	vsprintf(str, fmt, args);
	va_end(args);

	int width = strlen(str) + 5;
	WINDOW* textBox = newwin(3,width,0, menuWidth + 3);
	box(textBox,0,0);
	
	if(textColor) wattron(textBox, COLOR_PAIR(textColor));
	mvwprintw(textBox,1,1,str);
	if(textColor) wattroff(textBox, COLOR_PAIR(textColor));
	

	wrefresh(textBox);
	refresh();
	if(pause) getch();
	free(str);
	return textBox;
}

char* getInput(char* title, char* questionFmt, ...){

	va_list args;
	va_start(args, questionFmt);
	char *str = calloc(120, sizeof(char));
	vsprintf(str, questionFmt, args);
	va_end(args);

	WINDOW* win = createSecondary(title,5,strlen(str) + 10,menuWidth+2,0);
	char tmp[100];
	mvwprintw(win,1,1,str);
	echo();
	curs_set(1);
	mvwscanw(win,3,4,"%s",&tmp);

	noecho();
	curs_set(0);
	char* input = malloc(sizeof(char) * strlen(tmp));
	strcpy(input,tmp);
	free(str);
	deleteWindow(win);
	return input;
}

bool yesOrNo(char* msg){
	int width = strlen(msg) + 5;
	WINDOW* win = newwin(6,width,0, menuWidth+3);
	box(win,0,0);
	mvwprintw(win,1,1,msg);
	wrefresh(win);

	keypad(win,true);
	int ch=0;
	mvwprintw(win, 3,2,"yes");
	mvwprintw(win, 4,2,"no");

	bool yesSel = false;
	do{
		mvwprintw( win,3, 2, "yes");
		mvwprintw( win,4, 2, "no");
		if(ch == KEY_UP || ch == KEY_DOWN){
			yesSel = !yesSel;
		}

		wattron( win, A_STANDOUT );
		if(yesSel){
			mvwprintw( win,3, 2, "yes");
		}else{
			mvwprintw( win,4, 2, "no");
		}
		wattroff( win, A_STANDOUT );
	}while((ch = wgetch(win)) != '\n');

	deleteWindow(win);
	return yesSel;
}

int buildMenu(){
	WINDOW* menu = newwin(7,20,0,menuWidth+2);
	box(menu,0,0);
	mvwprintw(menu,1,1,"Network Structure");
	wrefresh(menu);

	keypad(menu,true);

	mvwprintw(menu, 3,2,"Constant");
	mvwprintw(menu, 4,2,"Tapered");
	mvwprintw(menu, 5,2,"Custom");

	int choice = 0;

	int ch = 0;
	do{
		mvwprintw(menu, 3,2,"Constant");
		mvwprintw(menu, 4,2,"Tapered");
		mvwprintw(menu, 5,2,"Custom");

		if(ch == KEY_UP){
			choice--;
			if(choice < 0) 
				choice = 2;
		}else if(ch == KEY_DOWN){
			choice++;
			if(choice > 2)
				choice = 0;
		}

		wattron(menu, A_STANDOUT );
		if(choice == CONSTANT){
			mvwprintw(menu, 3,2,"Constant");
		}else if(choice == TAPER){
			mvwprintw(menu, 4,2,"Tapered");
		}else{
			mvwprintw(menu, 5,2,"Custom");
		}
		wattroff(menu, A_STANDOUT );
	}while((ch = wgetch(menu)) != '\n');

	deleteWindow(menu);
	return choice;


}

void buildNew(){
	int choice = buildMenu();

	WINDOW *secondary, *msgbox;
	secondary = createSecondary("New", 16, menuWidth, menuWidth+2,0);

	bool success = false;
	if(choice == CONSTANT){
		success = newConstantNetSetup(secondary);
	}else if(choice == TAPER){
		success = newTaperedNetSetup(secondary);
	}else{
		success = newCustomNetSetup(secondary);
	}
	
	if(success){
		deleteWindow(secondary);
		msgbox = textBox(false, NO_COLOR, "Building...");
		build();
		deleteWindow(msgbox);
		msgbox = textBox(false, NO_COLOR, "Randomizing...");
		randomizeAllWeights();
		//randomInputs();
		deleteWindow(msgbox);
		alert( GREEN, "Built!", NULL);
		netLoaded = true;
	}else{
		deleteWindow(secondary);
		alert( RED,"Build failed.", NULL);
		netLoaded = false;
	}
}

char* saveWindow(){
	curs_set(1);
    char name[100] = "networks/";
	char* tmp = getInput("Save","File Name?");

	strcat(name,tmp);
	strcat(name,".nn");
	char* fname = calloc(100,sizeof(char));
	strcpy(fname,name);

	return fname;

}

void save(){
	char* saveName;
	saveName = saveWindow();
	WINDOW* saving = textBox(false, NO_COLOR, "Saving to %s",saveName);
	saveNet(saveName);
	deleteWindow(saving);
	free(saveName);
	alert( GREEN, "Network Saved!", NULL);
}

char* loadWindow(WINDOW* win, char** files, int count){
	keypad(win,true);

	mvwprintw(win,1,1,"Choose a file to load");
	for(int i=0; i<count; i++){
		if(i==0)
			wattron(win,A_STANDOUT);
		else
			wattroff(win,A_STANDOUT);
		mvwprintw(win,i+3,2,files[i]);
	}
	wrefresh(win);

	char item[30];
	int ch;
	int i=0;
	while ((ch = wgetch(win)) != '\n'){
		sprintf(item, "%-4s", files[i]);
		mvwprintw(win, i+3, 2, "%s", item);
		switch (ch){
			case KEY_UP:
				i--;
				i = (i < 0) ? count-1 : i;
				break;
			case KEY_DOWN:
				i++;
				i = (i > count-1) ? 0 : i;
				break;
		}

		wattron( win, A_STANDOUT );
		sprintf(item, "%-4s",  files[i]);
		mvwprintw( win, i+3, 2, "%s", item);
		wattroff( win, A_STANDOUT );
	}

	char* choice = calloc(30,sizeof(char));
	strcpy(choice,files[i]);
	return choice;
}

void load(){
	WINDOW *secondary, *msgbox;	
	struct fileNameData* data = loadFileNames();
	if(!data){
		alert(RED,"No .nn files in networks/ directory!", NULL);
		return;
	}
	int filecount = data->count;
	char** fileNames = data->fileNames;
	int lines = (filecount > 14) ? 14 : filecount + 4;
	secondary = createSecondary("Load", lines, menuWidth, menuWidth + 2, 0);
	char* file = loadWindow(secondary, fileNames, filecount);
	deleteWindow(secondary);
	msgbox = textBox(false, NO_COLOR, "loading...");
	loadNet(file);
	free(file);
	deleteWindow(msgbox);
	alert( GREEN,"Loaded!", NULL);
	exitdir(fileNames,data->dir,filecount);
	free(data);
	netLoaded = true;
}


void trainInput(){
	char* tmpIn = malloc(1);
	char* tmpOut = malloc(1); 
	tmpIn = getInput("Input","Enter a input sentence");
	tmpOut = getInput("Expected", "Enter an expected output");
	WINDOW* status = textBox(false,NO_COLOR,"Training...");
	train(stringToFloatArr(tmpIn),stringToFloatArr(tmpOut));
	deleteWindow(status);
	alert(GREEN,"Done!",NULL);
	free(tmpIn);
	free(tmpOut);
}

WINDOW* updateNodeInfo(WINDOW* win, int layer, int nodeIndex){
	win = newwin(9,menuWidth,listNum+18,0);
	box(win,0,0);
	wprintw(win,"[%d] [%d]",layer,nodeIndex);

	mvwprintw(win,1,1,"ID: %d",Network.net[layer][nodeIndex].id);

	mvwprintw(win,3,1,"Value: %.4f",Network.net[layer][nodeIndex].val);

	mvwprintw(win,5,1,"Incoming edges: %d",Network.net[layer][nodeIndex].numIncoming);

	mvwprintw(win,7,1,"Outgoing edges: %d",Network.net[layer][nodeIndex].numOutgoing);

	wrefresh(win);
	return win;
}

WINDOW* netStatsWindow(){
	WINDOW* win = createSecondary("Stats",16, menuWidth, 0 ,listNum+2);
	mvwprintw(win,1,1,"Inputs:");
	mvwprintw(win,2,2,"- %d",Network.inputs);

	mvwprintw(win,4,1,"Outputs:");
	mvwprintw(win,5,2,"- %d",Network.outputs);

	mvwprintw(win,7,1,"Total Nodes:");
	mvwprintw(win,8,2,"- %d",Network.totalNodes);

	mvwprintw(win,10,1,"Total Edges:");
	mvwprintw(win,11,2,"- %d",Network.totalEdges);

	char* sizeStr = bytesToReadable(networkSize());
	mvwprintw(win,13,1,"Network Size:");
	mvwprintw(win,14,2,"- %s",sizeStr);
	free(sizeStr);

	wrefresh(win);
	return win;
}

void edgeWeightView(node* n){
	int larger = (n->numIncoming > n->numOutgoing)? n->numIncoming : n->numOutgoing;
	WINDOW* win = newwin(larger+2,20,3,menuWidth+4);
	box(win,0,0);

	for(int i=0; i<n->numIncoming; i++){
		mvwprintw(win,i+1,1,"%.4f",n->incomingEdges[i]->weight);
	}

	for(int b=0; b<larger; b++){
		mvwprintw(win,b+1,9,"|");
	}

	for(int o=0; o<n->numOutgoing; o++){
		mvwprintw(win,o+1,12,"%.4f",n->outgoingEdges[o]->weight);
	}

	wrefresh(win);
	getch();
}

void viewStructure(){
	WINDOW* stats = netStatsWindow();
	WINDOW* nodeInfo = NULL;
	getmaxyx(stdscr,yMax,xMax);

	int height = yMax - 3;
	WINDOW* msgbox = textBox(false, NO_COLOR, "press 'q' to exit network view, enter to switch mode. e to see edge weights while in node view.");
	WINDOW* view = newwin(height,xMax-menuWidth-5,3,menuWidth+3);
	box(view,0,0);

	keypad(view,true);

	int maxNpl = Network.inputs;
	for(int i=1; i<Network.numLayers; i++){
		if(Network.layerCount[i] > maxNpl) maxNpl = Network.layerCount[i];
	}

	int startx = (int)(xMax/2) - (int)(Network.numLayers/2) - 4;
	int starty = (int)(yMax/2) - (int)(maxNpl/2) - 6;
	int x = startx;
	int y = starty;
	int cursorX = 0, cursorY = 0;
	keypad(view,true);

	bool moveMode = true;

	int ch = 0;
	int speed = 1;
	int layerY = y;
	do{
		if (moveMode){
			for (int i = 0; i < Network.numLayers; i++){
				layerY = y - (Network.layerCount[i] / 2);
				for (int j = 0; j < Network.layerCount[i]; j++){
					mvwprintw(view, layerY + j, x + (i * 4), " ");
					if(i == 0){
						mvwprintw(view, layerY + j, x + (i * 4)-14,"              ");
					}
					if(i == Network.numLayers-1){
						mvwprintw(view, layerY + j, x + (i * 4)+1, "              ");
					}
				}
			}
			switch (ch){
				case KEY_LEFT:
					x -= speed;
					break;
				case KEY_RIGHT:
					x += speed;
					break;
				case KEY_DOWN:
					y += speed;
					break;
				case KEY_UP:
					y -= speed;
					break;
				case '=':
					speed++;
					break;
				case '-':
					speed--;
					break;
				case '\n':
					moveMode = false;
					cursorX = 0;
					cursorY = Network.layerCount[0] - 1;
					nodeInfo = updateNodeInfo(nodeInfo,0,Network.layerCount[0] - 1);
				break;
			}

		}else{	//info mode
			switch (ch){
				case KEY_LEFT:
					cursorX = (cursorX <= 0)? 0 : cursorX - 1;
					cursorY = Network.layerCount[cursorX] - 1;
					break;
				case KEY_RIGHT:
					cursorX = (cursorX >= Network.numLayers-1)? Network.numLayers-1 : cursorX + 1;
					cursorY = Network.layerCount[cursorX] - 1;
					break;
				case KEY_UP:
					cursorY = (cursorY <= 0)? 0 : cursorY - 1;
					break;
				case KEY_DOWN:
					cursorY = (cursorY >= Network.layerCount[cursorX]-1)? Network.layerCount[cursorX]-1 : cursorY + 1;
					break;
				case 'e':
					edgeWeightView(&Network.net[cursorX][cursorY]);
					break;
				case '\n':
					moveMode = true;
					werase(nodeInfo);
					wrefresh(nodeInfo);
					break;
			}
			if(!moveMode) nodeInfo = updateNodeInfo(nodeInfo,cursorX,cursorY);
		}
		for (int i = 0; i < Network.numLayers; i++){
			layerY = y - (Network.layerCount[i] / 2);
			for (int j = 0; j < Network.layerCount[i]; j++){
				if(!moveMode && (cursorX == i && cursorY == j)) wattron(view,A_STANDOUT);
					float tmpval = Network.net[i][j].val;
					if(i == 0){
						mvwprintw(view, layerY + j, (x + (i * 4)) - 14, "%.4f - %c -", tmpval, floatToChar(tmpval));
					}
					if(i == Network.numLayers-1){
						mvwprintw(view, layerY + j, (x + (i * 4))+1, " - %.4f - %c", tmpval, floatToChar(tmpval));
					}
				mvwprintw(view, layerY + j, x + (i * 4), "O");
				wattroff(view,A_STANDOUT);
			}
		}
		box(view, 0, 0);
	}while ((ch = wgetch(view)) != 'q');

	deleteWindow(nodeInfo);
	deleteWindow(msgbox);
	deleteWindow(stats);
	wrefresh(view);
	deleteWindow(view);
}


void settingsWindow(){	
	#define numSettings 3

	WINDOW* secondary = createSecondary("Settings", numSettings+2, menuWidth, 0, listNum+2);


	keypad(secondary,true);	//allow arrow keys

	char list[numSettings][20] = {
		"Bias Value",
		"Learning Rate",
		"Back",
	};

	char item[20];

	for(int i=0;i<numSettings;i++){
		if(i==0)
			wattron(secondary,A_STANDOUT);
		else
			wattroff(secondary,A_STANDOUT);
		sprintf(item,"%-4s", list[i]);
		mvwprintw(secondary, i+1,2,"%s",item);
	}

	bool inSettings = true;
	int ch = 0;
	int i = 0;
	do{
		ch = wgetch(secondary);
		sprintf(item, "%-4s",  list[i]); 
		mvwprintw( secondary, i+1, 2, "%s", item ); 
		switch( ch ) {
			case KEY_UP:
				i--;
				i = ( i<0 ) ? numSettings-1 : i;
				break;
			case KEY_DOWN:
				i++;
				i = ( i>numSettings-1 ) ? 0 : i;
				break;
			case '\n':

				#define BIAS  0
				#define LEARNRATE 1
				#define BACK  2

				sprintf(item, "%-4s",  list[i]);
				wrefresh(secondary);
				switch(i){
					case BIAS:
						Network.bias = atof(getInput("Bias","Enter Bias value (currently %.2f)", Network.bias));
						break;
					case LEARNRATE:
						Network.learnRate = atof(getInput("Learning Rate", "Enter Learning Rate (currently %.4f)",Network.learnRate));
						break; 
					case BACK : inSettings = false;
						break;
				}
		}
		wattron( secondary, A_STANDOUT );

		sprintf(item, "%-4s",  list[i]);
		mvwprintw( secondary, i+1, 2, "%s", item);
		wattroff( secondary, A_STANDOUT );
	}while(inSettings);


	deleteWindow(secondary);
}


WINDOW* menu;

void quitGui(){
	WINDOW* quit = textBox(false, NO_COLOR, "Exiting...");
	if(netLoaded) freeNet();
	delwin(menu);
	deleteWindow(quit);
	endwin();
	exit(1);
}

void exitHandler(){
	if(yesOrNo("Are you sure you want to quit?")){
		quitGui();
	}
	return;
}

void runGui(){	
	initscr(); 
	noecho();
	cbreak();

	getmaxyx(stdscr,yMax,xMax);

	char list[listNum][20] = {
		"Build New Network",
		"Save Network",
		"Load Network",
		"Input",
		"Train",
		"View Network",
		"Settings",
		"Quit",
	};

	menu = newwin(listNum+2,menuWidth,0,0);
	char item[20];
	int ch, i=0;
	int size;
	char* str;
	box(menu,0,0);
	refresh();

	wprintw(menu,"CANN");
	for(i=0;i<listNum;i++){
		if(i==0)
			wattron(menu,A_STANDOUT);
		else
			wattroff(menu,A_STANDOUT);
		sprintf(item,"%-4s", list[i]);
		mvwprintw(menu, i+1,2,"%s",item);
	}

	wrefresh(menu);

	i=0;
	keypad(menu,true);	//allow arrow keys
	curs_set(0);

	while((ch = wgetch(menu)) != 'q'){
		sprintf(item, "%-4s",  list[i]); 
		mvwprintw( menu, i+1, 2, "%s", item ); 
		switch( ch ) {
			case KEY_UP:
				i--;
				i = ( i<0 ) ? listNum-1 : i;
				break;
			case KEY_DOWN:
				i++;
				i = ( i>listNum-1 ) ? 0 : i;
				break;
			case '\n':
				sprintf(item, "%-4s",  list[i]);
				mvwprintw( menu, i+1, 2, "%s", item ); //unhighlight current selection for new window
				wrefresh(menu);
				switch(i){
					case BUILD_NEW:
						if(netLoaded){
							if(yesOrNo("This will overwrite current network. Continue?")){
								freeNet();
								netLoaded = false;
								buildNew();
							}
						}else {
							buildNew();
						}
						break;
					case SAVE:
						if(netLoaded)
							save();
						else alert( RED, "No Network in memory.", NULL);
						break;
					case LOAD:
						if(netLoaded){
							if(yesOrNo("This will overwrite current network. Continue?")){
								freeNet();
								netLoaded = false;
								load();
							}
						}else {
							load();
						}
						break;
					case INPUT:
						if (netLoaded){
							str = getInput("Input","Enter an input to test");
							size = strlen(str);
							alert(GREEN,"Output: %s",calcInput(stringToFloatArr(str),size));
						}else alert(RED, "No Network in memory.", NULL);
						break;
					case TRAIN:
						if (netLoaded)
							trainInput();
						else alert(RED, "No Network in memory.", NULL);
						break;
					case VIEW:
						if(netLoaded)
							viewStructure();
						else alert( RED, "No Network in memory.", NULL);
						break;
					case SETTINGS:
						settingsWindow();
						break;
					case QUIT:
						quitGui(menu);
						break;
				}
				break;
		}
		wattron( menu, A_STANDOUT );

		sprintf(item, "%-4s",  list[i]);
		mvwprintw( menu, i+1, 2, "%s", item);
		wattroff( menu, A_STANDOUT );
	}

	wrefresh(menu);
	quitGui(menu);
}

int main(){
	initColor();
	signal(SIGINT,exitHandler);
	runGui();
	return 0;
}
