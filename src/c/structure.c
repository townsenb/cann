#include <cann.h>

#include <fileio.h>
#include <netutils.h>
#include <structure.h>
#include <gui.h>


void build(){
  
    Network.net = (node**) malloc(sizeof(node*) * Network.numLayers);
    for(int i=0; i < Network.numLayers; i++){
        Network.net[i] = (node*) calloc(Network.layerCount[i],sizeof(node));
    }

    Network.totalEdges = 0;
    Network.learnRate = 0.25;
    int idcount = 0;
    for(int i=0;i<Network.numLayers;i++){
        for(int j=0;j<Network.layerCount[i];j++){
            node* tempNode = &Network.net[i][j];
            tempNode->id = idcount;
            idcount++;
            if(i == 0){
                tempNode->numIncoming = 0;  //inputs
                tempNode->numOutgoing = Network.layerCount[i+1];

                tempNode->outgoingEdges = (edge**) 
                    calloc(tempNode->numOutgoing, sizeof(edge*));

                if(tempNode->outgoingEdges == NULL) {end(29);}

            }else if(i == Network.numLayers-1){
                tempNode->numOutgoing = 0;  //outputs
                tempNode->numIncoming = Network.layerCount[i-1];

                tempNode->incomingEdges = (edge**) 
                    calloc(tempNode->numIncoming, sizeof(edge*));
                
                if(tempNode->incomingEdges == NULL) {end(38);}

            }else{ //hidden layers
                tempNode->numIncoming = Network.layerCount[i-1];
                tempNode->numOutgoing = Network.layerCount[i+1];
 
                tempNode->incomingEdges = (edge**) 
                    calloc(tempNode->numIncoming, sizeof(edge*));
                if(tempNode->incomingEdges == NULL) {end(46);}

                tempNode->outgoingEdges = (edge**) 
                    calloc(tempNode->numOutgoing,sizeof(edge*));
                if(tempNode->outgoingEdges == NULL) {end(50);}
            }
        }        
    }

    for(int i=0;i<Network.numLayers;i++){
        for(int j=0;j<Network.layerCount[i];j++){
            for(int e=0;e<Network.net[i][j].numOutgoing;e++){
                connectEdge(&Network.net[i][j],&Network.net[i+1][e],e,j);
                Network.totalEdges++;
            }
        }
    }

    Network.totalNodes = idcount;
    return;
}

void connectEdge(node* from, node* to, int outIndex, int inIndex){
    edge* newEdge = calloc(1,sizeof(edge));
    newEdge->firing = false;
    newEdge->weight = 0.0;
    newEdge->fromNode = from;
    newEdge->toNode = to;

    
    from->outgoingEdges[outIndex] = newEdge;
    to->incomingEdges[inIndex] = newEdge;
}


void setupLayers(int numLayers, int* layerCounts){

    Network.layerCount = calloc(numLayers,sizeof(int));

    for(int i=0; i < numLayers; i++){
        Network.layerCount[i] = layerCounts[i];
    }

    Network.numLayers = numLayers;
    Network.hiddenLayers = numLayers - 2;
    Network.inputs = layerCounts[0];
    Network.outputs = layerCounts[numLayers-1];

    free(layerCounts);
}


void sameSizeHidden(int inputs, int outputs, int hiddenLayers, int nodesPerLayer){

    int* layerCounts = malloc(sizeof(int) * (hiddenLayers + 2));
    
    layerCounts[0] = inputs;
    layerCounts[hiddenLayers+1] = outputs;

    for(int i=1; i<hiddenLayers+1; i++){
        layerCounts[i] = nodesPerLayer;
    }

    setupLayers(hiddenLayers+2, layerCounts);
}

void linearChangeHidden(int inputs, int outputs, int hiddenLayers, int start, int end){
    bool decreasing = (start > end);
    float delta = ((float)(end-start)) / ((float)hiddenLayers);
    float decimal = delta - (int)delta;
    if(decimal < 0) decimal *= -1;

    int* layerCounts = malloc(sizeof(int) * (hiddenLayers + 2));

    layerCounts[0] = inputs; 
    layerCounts[hiddenLayers+1] = outputs; 

    int count = start;
    float remainder = 0.0;
    for(int i=1; i < hiddenLayers+1; i++){
        remainder += decimal;
        if(remainder >= 1.0){
            remainder = 0.0;
            count += (decreasing)? -1 : 1;
        }
        layerCounts[i] = count;
        count += (int)delta;
    }
	layerCounts[hiddenLayers] = end;
    setupLayers(hiddenLayers+2, layerCounts);
}

void end(int lineNum){
    freeNet();
    printf("Malloc failed. Line: %d\n",lineNum);
    exit(1);
}

node* getNodeById(int id){
    int previousCount = 0;
    for(int layer = 0; layer < Network.numLayers; layer++){
        if(id < (Network.layerCount[layer] + previousCount - 1)){
            for(int node = 0; node < Network.layerCount[layer]; node++){
                if(Network.net[layer][node].id == id){
                    return &Network.net[layer][node];
                }
            }
        }else{
            previousCount += Network.layerCount[layer];
        }
    }
    return NULL;  
}

edge* getEdgeByCount(int count){    
    int past = 0;
    for(int layer=0; layer < Network.numLayers-1; layer++){
        past += getOutEdgesPerLayer(layer);   
        if(count <= past){  // single out layer
            int remainder = count % Network.layerCount[layer+1];
            int nodeLocation = (count-past) / Network.layerCount[layer];

            return Network.net[layer][nodeLocation].outgoingEdges[remainder];
        }  
    }
    return NULL;
}

int getOutEdgesPerLayer(int layer){
    return (layer >= Network.numLayers)? -1 : Network.layerCount[layer] * Network.layerCount[layer+1];
}

int networkSize(){
    return (sizeof(edge) * Network.totalEdges) + (sizeof(node) * Network.totalNodes);
}

void freeNet(){
    for(int i=0; i<Network.numLayers; i++){
        for(int j=0;j<Network.layerCount[i];j++){
            free(Network.net[i][j].incomingEdges);
            free(Network.net[i][j].outgoingEdges);
        }
    }

    for(int i=0; i<Network.numLayers;i++){
        free(Network.net[i]);
    }    

    free(Network.layerCount);
    free(Network.net);
}
