#include <cann.h>

#include <fileio.h>
#include <netutils.h>
#include <structure.h>
#include <gui.h>
#include <ncursesutils.h>


void deleteWindow(WINDOW* win){
	werase(win);
	wrefresh(win);
	delwin(win);
	refresh();
	return;	
}

void initColor(){	
	start_color();
	use_default_colors();
	init_pair(GREEN,COLOR_GREEN,COLOR_BLACK);
	init_pair(RED,COLOR_RED,COLOR_BLACK);
}

void inputFail(char* buf){
	alert(RED,"Please only input numbers!", NULL);
	Network.inputs = Network.hiddenLayers = Network.nodesPerLayer = Network.outputs = 0;
	curs_set(0);
	free(buf);
}


bool isNumeric(char* str){
	if(str == NULL || strcmp(str,"") == 0) return false;
	int len = strlen(str);
	char c;
	for(int i=0; i<len; i++){
		c = str[i];
		if(!isdigit(c) || c == ' ' || c == '\0'){
			return false;
		}
	}
	return true;
}

bool newCustomNetSetup(WINDOW *win){
    curs_set(1);
    char *tmp = calloc(12, sizeof(char));

    mvwprintw(win, 1, 1, "# Hidden Layers? ");
    echo();
    mvwscanw(win, 2, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    int hl = atoi(tmp);

    int *layerCounts = malloc(sizeof(int) * hl + 2);

    mvwprintw(win, 4, 1, "# Inputs? ");
    echo();
    mvwscanw(win, 5, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    layerCounts[0] = atoi(tmp);

    for (int i = 1; i < hl + 1; i++)
    {
        mvwprintw(win, 7, 1, "# in Layer %d", i);
        echo();
        mvwscanw(win, 8, 4, "%s", tmp);
        if (!isNumeric(tmp))
        {
            inputFail(tmp);
            return false;
        }
        layerCounts[i] = atoi(tmp);
        wmove(win,8,4);
        wclrtoeol(win);
        box(win, 0, 0);
    }

    mvwprintw(win, 10, 1, "# Outputs? ");
    echo();
    mvwscanw(win, 11, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    layerCounts[hl + 1] = atoi(tmp);

    free(tmp);

    setupLayers(hl + 2, layerCounts);

    noecho();
    curs_set(0);
    return true; //succeed
}

bool newTaperedNetSetup(WINDOW *win){
    curs_set(1);
    char *tmp = calloc(12, sizeof(char));

    int in, out, hl, start, end;

    mvwprintw(win, 1, 1, "# Inputs? ");
    echo();
    mvwscanw(win, 2, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    in = atoi(tmp);

    mvwprintw(win, 4, 1, "# Outputs?");
    echo();
    mvwscanw(win, 5, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    out = atoi(tmp);

    mvwprintw(win, 7, 1, "# Start?");
    echo();
    mvwscanw(win, 8, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    start = atoi(tmp);

    mvwprintw(win, 10, 1, "# End?");
    echo();
    mvwscanw(win, 11, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    end = atoi(tmp);

    mvwprintw(win, 13, 1, "# Hidden Layers?");
    echo();
    mvwscanw(win, 14, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    hl = atoi(tmp);

    free(tmp);

    linearChangeHidden(in, out, hl, start, end);

    noecho();
    curs_set(0);
    return true; //succeed
}

bool newConstantNetSetup(WINDOW *win){
    curs_set(1);
    char *tmp = calloc(12, sizeof(char));

    int in, hl, npl, out;

    mvwprintw(win, 1, 1, "# Inputs? ");
    echo();
    mvwscanw(win, 2, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    in = atoi(tmp);

    mvwprintw(win, 4, 1, "# Hidden Layers?");
    echo();
    mvwscanw(win, 5, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    hl = atoi(tmp);

    mvwprintw(win, 7, 1, "# Nodes/Layer?");
    echo();
    mvwscanw(win, 8, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    npl = atoi(tmp);

    mvwprintw(win, 10, 1, "# Outputs?");
    echo();
    mvwscanw(win, 11, 4, "%s", tmp);
    if (!isNumeric(tmp))
    {
        inputFail(tmp);
        return false;
    }
    out = atoi(tmp);

    free(tmp);

    sameSizeHidden(in, out, hl, npl);

    noecho();
    curs_set(0);
    return true; //succeed
}