#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include <cann.h>
#include <netutils.h>


static void test_updateWeight(void** state){
    edge* e = malloc(sizeof(edge));
    e->firing = false;
    e->fromNode = NULL;
    e->toNode = NULL;
    e->weight = 0.5;
    assert_true(e->weight == 0.5);

    updateWeight(e,1.3);
    assert_true(e->weight == 1.3);
}

static void test_bytes_readable_bytes(void** state){
    assert_string_equal(bytesToReadable(0),"0.00 B");
    assert_string_equal(bytesToReadable(111),"111.00 B");
    assert_string_equal(bytesToReadable(999),"999.00 B");
}

static void test_bytes_readable_kilobytes(void** state){
    assert_string_equal(bytesToReadable(2111),"2.11 K");
    assert_string_equal(bytesToReadable(1000),"1.00 K");
    assert_string_equal(bytesToReadable(999999),"1000.00 K");    //round up
}

static void test_bytes_readable_megabytes(void** state){
    assert_string_equal(bytesToReadable(3222111),"3.22 M");
    assert_string_equal(bytesToReadable(1000000),"1.00 M");
    assert_string_equal(bytesToReadable(999999999),"1000.00 M"); //round up
}

static void test_bytes_readable_gigabytes(void** state){
    assert_string_equal(bytesToReadable(4333222111),"4.33 G");
    assert_string_equal(bytesToReadable(1000000000),"1.00 G");
    assert_string_equal(bytesToReadable(999999999999),"1000.00 G");  //round up
}

int main(){
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_updateWeight),

        cmocka_unit_test(test_bytes_readable_bytes),
        cmocka_unit_test(test_bytes_readable_kilobytes),
        cmocka_unit_test(test_bytes_readable_megabytes),
        cmocka_unit_test(test_bytes_readable_gigabytes),
    };

    return cmocka_run_group_tests(tests,NULL,NULL);
}
