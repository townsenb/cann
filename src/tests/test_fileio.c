#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include <cann.h>
#include <fileio.h>
#include <structure.h>


static void test_save_load_preserves_state(void** state){
    sameSizeHidden(3,3,2,5);
    build();

    struct neuralnet* save = &Network;

    char name[100] = "src/tests/testing_data/testnet.nn";

    saveNet(name);
    freeNet();

    loadNet(name);
    build();

    assert_memory_equal(save,&Network,sizeof(struct neuralnet));
}

int main(){
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_save_load_preserves_state),
    };

    return cmocka_run_group_tests(tests,NULL,NULL);
}
