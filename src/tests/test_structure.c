#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include <cann.h>
#include <structure.h>





static void test_build_basics(void** state){
    sameSizeHidden(5,1,2,5);
    build();

    assert_non_null(Network.net);
    assert_true(Network.inputs == 5);
    assert_true(Network.outputs == 1);
    assert_true(Network.hiddenLayers == 2);
    assert_true(Network.numLayers == 4);

    freeNet();
}

static void test_build_basics_bigger(void** state){
    sameSizeHidden(100,100,200,200);
    build();

    assert_non_null(Network.net);
    assert_true(Network.inputs == 100);
    assert_true(Network.outputs == 100);
    assert_true(Network.hiddenLayers == 200);
    assert_true(Network.numLayers == 202);
    
    freeNet();
}

static void test_edge_connects_input_to_output(void** state){
    sameSizeHidden(1,1,0,0);
    build();
    assert_non_null(Network.net);
    assert_true(Network.totalEdges == 1);

    node* input = &Network.net[0][0];
    node* output = &Network.net[1][0];
    assert_ptr_not_equal(input,output);

    node* travelNode = input->outgoingEdges[0]->toNode;

    assert_ptr_equal(travelNode,output);

    freeNet();
}

static void test_disconnect_io_no_hidden_layers(void** state){
    sameSizeHidden(1,1,1,0);
    build();
    assert_non_null(Network.net);
    assert_true(Network.totalEdges == 0);

    node* input = &Network.net[0][0];
    node* output = &Network.net[2][0];
    assert_ptr_not_equal(input,output);

    assert_true(input->numOutgoing == 0);
    assert_true(output->numIncoming == 0);

    edge* nullEdge = input->outgoingEdges[0];

    assert_null(nullEdge);

    freeNet();
}

static void test_networkSize(void** state){
    sameSizeHidden(1,1,1,1); // 3 nodes, 2 edges: o-o-o 
    build();

    assert_true(Network.totalNodes == 3);
    assert_true(Network.totalEdges == 2);

    int bytes = networkSize();
    int calcSize = sizeof(node) * 3 + sizeof(edge) * 2;

    assert_true(bytes == calcSize);
}

int main(){
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_build_basics),
        cmocka_unit_test(test_build_basics_bigger),
        cmocka_unit_test(test_edge_connects_input_to_output),
        cmocka_unit_test(test_disconnect_io_no_hidden_layers),

        cmocka_unit_test(test_networkSize),
    };

    return cmocka_run_group_tests(tests,NULL,NULL);
}


