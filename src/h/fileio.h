#ifndef FILEIO_H
#define FILEIO_H

#include "cann.h"

//returnable from loadFileNames()
struct fileNameData{       
    char** fileNames;   // array of strings of .nn files present 
    int count;          // number of .nn files in directory
    DIR* dir;           // freeable pointer (needed later)
};


/******************************************************************************
 * saveNet - saves current network state to disk.
 * param: fileName - string to name the file.
 * return:
 *****************************************************************************/
void saveNet(char* fileName);


/******************************************************************************
 * exitDir - frees all the file name pointers and closes the directory
 * param:  fileNames - freeable strings containing all the file names
 *          d - directory pointer for closing
 *          count - number of freeable file names in fileNames
 * return: 
 *****************************************************************************/
void exitdir(char** fileNames, DIR* d, int count);


/******************************************************************************
 * loadFileNames - retrieves files ending with .nn in the networks/ directory.
 * param:
 * return: struct containing array of file name strings, number of files,
 *          and a freeable directory pointer.
 *****************************************************************************/
struct fileNameData* loadFileNames();


/******************************************************************************
 * loadNet - reads numbers from .nn file and sets internal network values.
 * param: filename - string of file to load values from.
 * return: false on fail.
 *****************************************************************************/
bool loadNet(char* filename);


/******************************************************************************
 * loadWeights - set edge weights to values passed from a file load.
 * param: weights - array of edge weights in order.
 * return:
 * notes: having a large array holding the weights is very memory inefficient,
 *          hopefully will refactor loadNet() to fix this. 
 *****************************************************************************/
void loadWeights(FILE* file);


/******************************************************************************
 * loadDataFile - prototyping how loading in an input file could work.
 * param: file - file name to load into inputs.
 * return: expected output of file. (MNIST)
 *****************************************************************************/
int loadDataFile(char* file);

#endif