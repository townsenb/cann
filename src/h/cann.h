#ifndef CANN_H
#define CANN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <sys/resource.h>
#include <dirent.h>

#include <ncurses.h>        // point to absolute path of your ncurses.h file if you get a ton of undefined references during compile
                            // also make sure the linked file (-lncurses) in the makefile is the same name.

#define lui long unsigned int


typedef struct _edge edge;  // weighted connector between two neurons
typedef struct _node node;  // individual neuron

#pragma pack(1)

struct _node{   //32B
    int id;                 // node ID
    float val;              // current weighted sum of incoming edges
    int numIncoming;        // number of edges coming into this node
    int numOutgoing;        // number of edges leaving this node
    edge** incomingEdges;    // ptr to array of incoming edges
    edge** outgoingEdges;    // ptr to array of outgoing edges
};


struct _edge{   //25B
    bool firing;            // sets true when activation threshold is met
    double weight;          // current weight of edge
    node* fromNode;         // ptr to the node that this edge leaves from
    node* toNode;           // ptr to the node that this edge goes to
};

struct neuralnet{
    int inputs;             // number of inputs to the network, = layerCount[0]
    int outputs;            // number of outputs to the network, = layerCount[numLayers]
    int hiddenLayers;       // number of hidden layers between inputs and outputs
    int nodesPerLayer;      // number of nodes residing in each hidden layer
    int numLayers;          // total number of layers, = hiddenlayers + 2
    int* layerCount;        // array where layerCount[layer] is the number of nodes in that layer
    int totalNodes;         // total amount of nodes within the entire network
    int totalEdges;         // total amount of edges within the entire network
    float learnRate;        // learning rate for how fast weights are adjusted
    float bias; 
    node** net;             // individial node access, ex: Network.net[layer][depth]
};

struct neuralnet Network;   // global access to network

#endif //CANN_H


/* .nn File Format:
[bias]
[numLayers]
[inputs]
[layercount[1]]
...
[outputs]
[weight 1] 
[weight 2] 
...

example: 2 inputs, 1 HL with 1 nodes, 2 ouputs
2
2
1
1
0.854
0.488
0.670 
0.217
0.974 
0.386 
0.662
0.113 
0.730 
*/
