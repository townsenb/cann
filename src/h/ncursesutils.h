#ifndef NCURSESUTILS_H
#define NCURSESUTILS_H

#include "cann.h"


void deleteWindow(WINDOW* win);


void initColor();


void inputFail(char* buf);


bool isNumeric(char* str);


bool newCustomNetSetup(WINDOW *win);


bool newTaperedNetSetup(WINDOW *win);


bool newConstantNetSetup(WINDOW *win);


#endif