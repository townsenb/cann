#ifndef GUI_H
#define GUI_H

#include "cann.h"


/** macro to wrap deleteWindow(textBox(true,"")) **/
#define alert(color,fmt, ...) deleteWindow(textBox(true,color,fmt, __VA_ARGS__))


/******************************************************************************
 * runGui - start the main menu loop of the ncurses gui.
 * param:
 * return: 
 *****************************************************************************/
void runGui();


/******************************************************************************
 * textBox - create a textBox containing any useful status info. (variadic)
 * param:   pause - wait for user input before continuing.
 *          textColor - text color. (0-no color, 1-green, 2-red)
 *          fmt - format string for variadic parameters.
 * return: window pointer for later deletion.
 *****************************************************************************/
WINDOW* textBox( bool pause, int textColor, char* fmt, ... );



/******************************************************************************
 * deleteWindow - removes window from screen and frees assoc. memory.
 * param: win - pointer to window to be deleted.
 * return: 
 *****************************************************************************/
void deleteWindow(WINDOW* win);

/** COLOR DEFINITIONS **/
#define NO_COLOR 0
#define GREEN 1
#define RED 2

#endif