#ifndef STRUCTURE_H
#define STRUCTURE_H

#include "cann.h"



/******************************************************************************
 * build - creates network infrastructure based on input values.
 * param:
 * return: 
 *****************************************************************************/
void build();


/******************************************************************************
 * connectEdge - creates a new edge and connects the two given nodes with it.
 * param:   from - ptr to the node from which the edge leaves.
 *          to - ptr to the node to which the edge enters.
 *          outIndex - index of edge from the from-node.
 *          inIndex -  index of edge from the to-node.
 * return: 
 *****************************************************************************/
void connectEdge(node* from, node* to, int outIndex, int inIndex);


/******************************************************************************
 * sameSizeHidden - sets up layers for constant NodesPerLayer.
 * param:   inputs - number of inputs into the network.
 *          outputs - number of outputs from the network.
 *          hiddenLayers - number of layers between inputs and outputs.
 *          nodesPerLayer - number of nodes for each hidden layer.
 * return: 
 *****************************************************************************/
void sameSizeHidden(int inputs, int outputs, int hiddenLayers, int nodesPerLayer);


/******************************************************************************
 * linearChangeHidden - sets up layers for a tapered hidden layer shape.
 * param:   inputs - number of inputs into the network.
 *          outputs - number of outputs from the network.
 *          hiddenLayers - number of layers between inputs and outputs.
 *          start - number of nodes on the first hidden layer (layerCount[1]).
 *          end - number of nodes on the last hidden layer (layerCount[last-1]).
 * return: 
 *****************************************************************************/
void linearChangeHidden(int inputs, int outputs, int hiddenLayers, int start, int end);

/******************************************************************************
 * setupLayers - granular control of amount of nodes per layer.
 * param: numLayers - total amount of layers in network.
 *          layerCounts - array holding layerCount information.
 * return: 
 *****************************************************************************/
void setupLayers(int numLayers, int* layerCounts);

/******************************************************************************
 * end - debug purposes, show what line malloc failed on (usually large nets).
 * param: lineNum - line that malloc failed on.
 * return: 
 *****************************************************************************/
void end(int lineNum);


/******************************************************************************
 * networkSize - calculate the size of the network based on edges and nodes.
 * param:
 * return: theoretical network size in bytes.
 *****************************************************************************/
int networkSize();


/******************************************************************************
 * getNodeById - retrieves pointer to the node with given ID.
 * param: id - ID number to search.
 * return: pointer to node with given ID.
 *****************************************************************************/
node* getNodeById(int id);


/******************************************************************************
 * getEdgeByCount - retrieves pointer to the edge in order.
 * param: count - count number to search.
 * return: pointer to edge from count.
 *****************************************************************************/
edge* getEdgeByCount(int count);


/******************************************************************************
 * getOutEdgesPerLayer - gets the number of outgoing edges per layer.
 * param: layer - layer to get outgoing edges from.
 * return: number of edges in that layer.
 *****************************************************************************/
int getOutEdgesPerLayer(int layer);

/******************************************************************************
 * freeNet - frees all nested allocations and network pointers.
 * param:
 * return: 
 *****************************************************************************/
void freeNet();

#endif