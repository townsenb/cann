#ifndef NETUTILS_H
#define NETUTILS_H

#include "cann.h"


/******************************************************************************
 * updateWeight - updates the weight of given edge.
 * param:  e - pointer to edge that is updated.
 *         weight - new value of edge to be updated .
 * return: 
 *****************************************************************************/
void updateWeight(edge* e, double weight);


/******************************************************************************
 * sigmoid - simple activation function: 
 * param: x - input to funciton
 * return: output to function
 *****************************************************************************/
float sigmoid(float x);


/******************************************************************************
 * weightedSum - adds up the sum of incoming edges for a node.
 * param: n - node pointer to calculate sum.
 * return: weighted sum.
 *****************************************************************************/
float weightedSum(node* n);


/******************************************************************************
 * fireNode - sets all edge.firing in given node to true.
 * param: n - pointer to fireable node.
 * return: 
 *****************************************************************************/
void fireNode(node* n, bool state);


/******************************************************************************
 * bytesToReadable - converts bytes to human readable format
 * param: bytes - number of bytes
 * return: string of bytes, signified by B, K, M, or G
 *****************************************************************************/
char* bytesToReadable(long bytes);


#define CHAR_MAX 128

/******************************************************************************
 * charToFloat - converts a char to a decimel 0-1.
 * param:   c - char to convert.
 * return: decimel value between 0 and 1.
 *****************************************************************************/
float charToFloat(char c);


/******************************************************************************
 * floatToChar - converts a float to a char.
 * param: f - float to convert.
 * return: character represented by given decimal value.
 *****************************************************************************/
char floatToChar(float c);


/******************************************************************************
 * stringToFloatArr - converts a string to an array of floats
 * param:  str - string to convert.
 * return: float array pointer
 *****************************************************************************/
float* stringToFloatArr(char* str);


/******************************************************************************
 * floatArrToString - converts a float array to a string.
 * param:   fptr - float array pointer to convert
 * return: string pointer
 *****************************************************************************/
char* floatArrToString(float* fptr, int size);


/******************************************************************************
 * randomizeAllWeights - loops through every edge and randomizes edge's val.
 * param:
 * return: 
 *****************************************************************************/
void randomizeAllWeights();

/******************************************************************************
 * randomInputs - randomizes values for each input node.
 * param:
 * return: 
 *****************************************************************************/
void randomInputs();

/******************************************************************************
 * setInputs - sets initial input values.
 * param: numInputs - number of inputs in the network.
 *        inputArr - array of each individual input value.
 * return: false on failure, true on success.
 *****************************************************************************/
bool setInputs(int numInputs, float* inputArr);

/******************************************************************************
 * adjust - recursively traverse and adjust edge weights.
 * param:  n - node pointer to adjust it's edges.
 *         delta - difference between expected and real values.
 * return: 
 *****************************************************************************/
void adjust(node *n, float delta);

/******************************************************************************
 * backprop - loops through outputs and adjusts edges.
 * param: expected - float array of expected output values.
 * return: 
 *****************************************************************************/
void backprop(float* expected);

/******************************************************************************
 * train - doesn't do a darn thing yet.
 * param:
 * return: 
 *****************************************************************************/
void train(float *inputs, float *expected);

/******************************************************************************
 * calcInput - doesn't do a darn thing yet.
 * param:
 * return: 
 *****************************************************************************/
char *calcInput(float *inputs, int size);

#endif