BIN=cann

CC=gcc
OPT=0
CLINK=-lncurses -lm
CWARN=-Wall -Wformat-overflow=0 -Wextra
CFLAGSNOWARN=-g -O$(OPTM) -Isrc/h
CFLAGS= $(CWARN) $(CFLAGSNOWARN)
CDIR=src/c
SRCFILES=$(wildcard $(CDIR)/*.c)
OBJDIR=obj
OBJS=$(patsubst $(CDIR)/%.c, $(OBJDIR)/%.o, $(SRCFILES))


clean: all
	@echo "cleaning"
	@rm -rf $(OBJDIR)
	@echo "done!"

all:$(BIN)

$(BIN): $(OBJS)
	@echo "linking"
	@$(CC) $(OBJS) -o $@ $(CLINK) $(CFLAGS)


$(OBJDIR)/%.o: $(CDIR)/%.c $(OBJDIR)
	@$(CC) -c $< -o $@ $(CFLAGS)
	
$(OBJDIR):
	@mkdir -p $(OBJDIR)
	@echo "compiling"




# ====[TESTING]=====
TESTFRAMEWORK=cmocka
TESTDIR=src/tests
TESTS=$(wildcard $(TESTDIR)/*.c)
TESTBINS=$(patsubst $(TESTDIR)/%.c, $(TESTDIR)/bin/%, $(TESTS))
IGNOREGUI=$(filter-out $(OBJDIR)/gui.o $(OBJDIR)/ncursesutils.o,$(OBJS))

test: buildtests
	@for test in $(TESTBINS) ; do echo "\n$$test" ; ./$$test ; done
	@rm -rf $(TESTDIR)/bin
	@rm -rf $(TESTDIR)/obj
	@rm  $(TESTDIR)/testing_data/*
	@rm -rf $(OBJDIR)

buildtests:	all $(TESTDIR)/bin $(TESTBINS)


$(TESTDIR)/bin/%: $(TESTDIR)/%.c 
	@$(CC) $< $(IGNOREGUI) -o $@ -l$(TESTFRAMEWORK) $(CLINK) $(CFLAGSNOWARN)
	
$(TESTDIR)/obj:
	@mkdir -p $@

$(TESTDIR)/bin:
	@mkdir -p $@





#clean: link
#	@rm $(OUTS)
#	@echo "done!"
#
#link: $(OUTS)
#	@$(CC) $(OUTS) $(CFLAGS) $(CLINK) -o $(BIN)
#
#compile:
#	@echo "compiling..."
#	@$(CC) src/c/fileio.c -o f.o -c $(CFLAGS)
#	@$(CC) src/c/structure.c -o s.o -c $(CFLAGS) 
#	@$(CC) src/c/netutils.c -o n.o -c $(CFLAGS)
#	@$(CC) src/c/ncursesutils.c -o ncu.o -c $(CFLAGS)
#	@$(CC) src/c/gui.c -o g.o -c $(CFLAGS)
#	
#	
#test: compile
#	@$(CC) $(OUTSNG) src/tests/TEST_SUITE.c -o test -Isrc/h -lcheck -lrt -lm -lpthread -lsubunit
#	@rm $(OUTS)
#	@./test
#	@rm test
#	
#
#valgrind: compile
#	valgrind --track-origins=yes --show-leak-kinds=all --show-reachable=yes --suppressions=./data/.ncurses.supp --leak-check=full ./cann
#
#benchmark: compile
#	$(CC) src/c/benchmark.c -e_entry f.o s.o n.o g.o -o benchmark $(CFLAGS)
#	rm $(OUTSNG)


#OUTSNG=f.o s.o n.o
#OUTS=$(OUTSNG) g.o ncu.o
